<?php

/**
 * Change user infos action
 *
 * @package Krypto
 * @author Ovrley <hello@ovrley.com>
 */

session_start();

require "../../../../../config/config.settings.php";
require $_SERVER['DOCUMENT_ROOT'].FILE_PATH."/vendor/autoload.php";
require $_SERVER['DOCUMENT_ROOT'].FILE_PATH."/app/src/MySQL/MySQL.php";
require $_SERVER['DOCUMENT_ROOT'].FILE_PATH."/app/src/App/App.php";
require $_SERVER['DOCUMENT_ROOT'].FILE_PATH."/app/src/App/AppModule.php";
require $_SERVER['DOCUMENT_ROOT'].FILE_PATH."/app/src/User/User.php";
require $_SERVER['DOCUMENT_ROOT'].FILE_PATH."/app/src/Lang/Lang.php";

try {

    // Load app modules
    $App = new App(true);
    $App->_loadModulesControllers();

    // Check if user is logged
    $User = new User();
    if (!$User->_isLogged()) {
        throw new Exception("User not logged", 1);
    }

    $Lang = new Lang($User->_getLang(), $App);



    $adminAction = true;

    if(isset($_POST['nuevaclaveretiro']) && !empty($_POST['nuevaclaveretiro'])){
      $User->_setpassword_retiros(htmlspecialchars($_POST['nuevaclaveretiro'], ENT_QUOTES, 'UTF-8'));
    }



    $User->_saveChange(true);

    die(json_encode([
      'error' => 0,
      'reload' => $adminAction,
      'msg' => $Lang->tr('Done !')
    ]));

} catch (Exception $e) {
    die(json_encode([
    'error' => 1,
    'msg' => $e->getMessage()
  ]));
}
