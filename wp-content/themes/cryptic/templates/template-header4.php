<header class="header4">
  <!-- BOTTOM BAR -->
  <nav class="navbar navbar-default" id="modeltheme-main-head">
    <div class="container">
        <div class="row">

          <!-- LOGO -->
          <div class="navbar-header col-md-2">

            <!-- NAVIGATION BURGER MENU -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div class="pull-right actions-group hide-on-desktop mobile-version">
              <?php echo cryptic_social_media_accounts(); ?>
              <?php echo cryptic_user_login_logout_links(); ?>
              <a href="#" class="mt-search-icon">
                <i class="fa fa-search" aria-hidden="true"></i>
              </a>
            </div>

            <?php 

            $custom_header_activated = get_post_meta( get_the_ID(), 'smartowl_custom_header_options_status', true );
            $header_v = get_post_meta( get_the_ID(), 'smartowl_header_custom_variant', true );
            $custom_logo_url = get_post_meta( get_the_ID(), 'smartowl_header_custom_logo', true );

            if($custom_header_activated == 'yes' && isset($custom_logo_url) && !empty($custom_logo_url)) { ?>

              <h1 class="logo">
                  <a href="<?php echo esc_url(get_site_url()); ?>">
                      <img src="<?php echo esc_url($custom_logo_url); ?>" alt="<?php echo esc_attr(get_bloginfo()); ?>" />
                  </a>
              </h1>

            <?php } else {

              if(cryptic_redux('mt_logo','url')){ ?>
                <h1 class="logo">
                    <a href="<?php echo esc_url(get_site_url()); ?>">
                        <img src="<?php echo esc_url(cryptic_redux('mt_logo','url')); ?>" alt="<?php echo esc_attr(get_bloginfo()); ?>" />
                    </a>
                </h1>
              <?php }else{ ?>
                <h1 class="logo no-logo">
                    <a href="<?php echo esc_url(get_site_url()); ?>">
                      <?php echo esc_html(get_bloginfo()); ?>
                    </a>
                </h1>
              <?php } ?>
            <?php } ?>
          </div>

          <!-- NAV MENU -->
          <?php $menu_class = 'col-md-9'; ?>
          <?php $pull_state = 'pull-right'; ?>
          <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
            <?php $menu_class = 'col-md-7'; ?>
            <?php $pull_state = 'pull-left'; ?>
          <?php } ?>

          <!-- NAV MENU -->
          <div id="navbar" class="navbar-collapse collapse <?php echo esc_attr($menu_class); ?>">
            <ul class="menu nav navbar-nav <?php echo esc_attr($pull_state); ?> nav-effect nav-menu">
              <?php
                // NAV METABOX
                $mt_page_custom_menu = get_post_meta( get_the_ID(), 'mt_page_custom_menu', true );

                // NAV ARRAY
                $defaults = array(
                  'menu'            => '',
                  'container'       => false,
                  'container_class' => '',
                  'container_id'    => '',
                  'menu_class'      => 'menu',
                  'menu_id'         => '',
                  'echo'            => true,
                  'fallback_cb'     => false,
                  'before'          => '',
                  'after'           => '',
                  'link_before'     => '',
                  'link_after'      => '',
                  'items_wrap'      => '%3$s',
                  'depth'           => 0,
                  'walker'          => ''
                );
                $defaults['theme_location'] = 'primary';

                // NAV LISTING
                if (isset($mt_page_custom_menu) && $mt_page_custom_menu != 'default' && $mt_page_custom_menu != '') {
                  echo wp_nav_menu( array('menu' => $mt_page_custom_menu ));
                }else{
                    if ( has_nav_menu( 'primary' ) ) {
                    echo wp_nav_menu( $defaults );
                  }else{
                    if( current_user_can( 'administrator' ) ){
                      echo '<p class="no-menu text-right">';
                        echo esc_html__('Primary navigation menu is missing. ', 'cryptic');
                      echo '</p>';
                    }
                  }
                }
              ?>
            </ul>
          </div>


          <?php $right_side_social = 'hidden'; ?>
          <?php if ( class_exists( 'ReduxFrameworkPlugin' ) ) { ?>
            <?php if(cryptic_redux('mt_header_fixed_sidebar_menu_status') == true || cryptic_redux('mt_header_is_search') == true) { ?>
              <?php $right_side_social = 'visibile_group'; ?>
            <?php } ?>
          <?php } ?>

          <!-- RIGHT SIDE SOCIAL / ACTIONS BUTTONS -->
          <div class="col-md-3 right-side-social-actions hide-on-mobile <?php echo esc_attr($right_side_social); ?>">

            <!-- ACTIONS BUTTONS GROUP -->
            <div class="pull-right actions-group">

              <?php if(cryptic_redux('mt_header_fixed_sidebar_menu_status') == true) { ?>
                <!-- MT BURGER -->
                <div id="mt-nav-burger">
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                  <span></span>
                </div>
              <?php } ?>


              <?php if(cryptic_redux('mt_header_is_search') == true){ ?>
                <!-- SEARCH ICON -->
                <a href="<?php echo esc_url('#'); ?>" class="mt-search-icon">
                  <i class="fa fa-search" aria-hidden="true"></i>
                </a>
              <?php } ?>

              <?php /*Lists login button / links depending if the user is logged in or out*/ ?>
              <?php echo cryptic_user_login_logout_links(); ?>

            </div>

            <?php /*Lists all active social media accounts from the theme panel*/ ?>
            <?php echo cryptic_social_media_accounts(); ?>
          </div>
        </div>
    </div>
  </nav>
</header>
