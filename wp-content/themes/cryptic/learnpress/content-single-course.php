<?php
/**
 * Template for displaying course content within the loop.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/content-single-course.php
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

if ( post_password_required() ) {
	echo get_the_password_form();

	return;
}

/**
 * @deprecated
 */
do_action( 'learn_press_before_main_content' );
do_action( 'learn_press_before_single_course' );
do_action( 'learn_press_before_single_course_summary' );

/**
 * @since 3.0.0
 */
do_action( 'learn-press/before-main-content' );

do_action( 'learn-press/before-single-course' );

?>
<div id="learn-press-course" class="course-summary">
	<?php
	/**
	 * @since 3.0.0
	 *
	 * @see learn_press_single_course_summary()
	 */
	do_action( 'learn-press/single-course-summary' );
	?>
</div>


<div class="related-posts sticky-posts">
    <h2 class="heading-bottom"><?php esc_html_e('Related Courses', 'cryptic'); ?></h2>
    <div class="row courses-list">
    <?php 
 
        $args=array(  
            'post__not_in'          => array(get_the_ID()),  
            'post_type'        		=> 'lp_course',
            'posts_per_page'        => 3,
            'post_status'      		=> 'publish' 
        );

        $my_query = new wp_query( $args );  

        while( $my_query->have_posts() ) {  
            $my_query->the_post();  

    ?>  
		<?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),'smartowl_post_pic700x450' ); ?>
		<?php $course_badge = get_post_meta( get_the_ID(), 'smartowl_course_badge_icon', true ); ?>
		<?php $course_background_color = get_post_meta( get_the_ID(), 'smartowl_course_background_color', true ); ?>
		<?php $course_border_color = get_post_meta( get_the_ID(), 'smartowl_course_border_color', true ); ?>

			    <div class="col-md-4 col-sm-4">
				    <div class="shortcode_course_content" style="background-color:<?php echo esc_attr($course_background_color); ?>; border-color: <?php echo esc_attr($course_border_color); ?>;">
				        <a href="<?php echo get_permalink(get_the_ID()); ?>">
				            <div class="featured_image_courses">
				            <div class="course_badge"><i class="<?php echo esc_attr($course_badge); ?>" style="background-color:<?php echo esc_attr($course_background_color); ?>"></i></div>
				                <?php if($thumbnail_src) { ?>
				                    <img src="<?php echo esc_url($thumbnail_src[0]); ?>" alt="<?php echo get_the_title(); ?>" />
				                <?php }else{ ?>
				                    <img src="http://placehold.it/500x300" alt="<?php echo get_the_title(); ?>" /> 
				                <?php } ?>
				            </div>
				        </a>
				        <div class="col-md-12 course_text_content">
				            <a href="<?php echo get_permalink(get_the_ID()); ?>">
				                <div class="col-md-12 course_text_container">
				                    <h4 class="course_title"><?php echo the_title(); ?></h4>
				                </div>
				            </a>
				        </div>
				    </div>
			    </div>

        <?php } ?>
    </div>

    <?php wp_reset_postdata(); ?>  

</div>
<?php

/**
 * @since 3.0.0
 */
do_action( 'learn-press/after-main-content' );
do_action( 'learn-press/after-single-course' );

/**
 * @deprecated
 */
do_action( 'learn_press_after_single_course_summary' );
do_action( 'learn_press_after_single_course' );
do_action( 'learn_press_after_main_content' );