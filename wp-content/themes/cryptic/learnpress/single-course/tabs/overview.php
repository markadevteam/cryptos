<?php
/**
 * Template for displaying overview tab of single course.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/single-course/tabs/overview.php.
 *
 * @author  ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();
?>

<?php global $course; ?>

<div class="course-description" id="learn-press-course-description">
	<div class="row">
		<div class="col-md-8">
			<h3 class="lms-posttitle title" id="learn-press-course-description-heading"><?php echo esc_html__('Course Overview','cryptic'); ?></h3>
			<?php
			/**
			 * @deprecated
			 */
			do_action( 'learn_press_begin_single_course_description' );

			/**
			 * @since 3.0.0
			 */
			do_action( 'learn-press/before-single-course-description' );

			echo wp_kses_post($course->get_content());

			/**
			 * @since 3.0.0
			 */
			do_action( 'learn-press/after-single-course-description' );

			/**
			 * @deprecated
			 */
			do_action( 'learn_press_end_single_course_description' );
			?>
		</div>
		<div class="col-md-4">
			<?php echo cryptic_lms_course_infos(); ?>
		</div>
	</div>
</div>