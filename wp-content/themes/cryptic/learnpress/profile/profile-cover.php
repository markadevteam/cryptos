<?php
/**
 * Template for displaying user profile cover image.
 *
 * This template can be overridden by copying it to yourtheme/learnpress/profile/profile-cover.php.
 *
 * @author   ThimPress
 * @package  Learnpress/Templates
 * @version  3.0.0
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit();

$profile = LP_Profile::instance();

$user = $profile->get_user();
$user_id = get_current_user_id();
$current_user = wp_get_current_user();
?>

<div id="learn-press-profile-header" class="lp-profile-header">
    <div class="lp-profile-cover">
		<div class="user-info" id="learn-press-user-info">
			<div class="user-basic-info">
				<div class="lms-learning-profile-metas">
					<span class="user-avatar"><?php echo wp_kses_post($user->get_profile_picture()); ?></span>
					<div class="clearfix"></div>
					<strong class="user-nicename"><?php echo esc_html($current_user->display_name); ?></strong>
					<?php if ( $description = get_user_meta( $user_id, 'description', true ) ): ?>
						<p class="user-bio"><?php echo get_user_meta( $user_id, 'description', true ); ?></p>
					<?php endif; ?>
					<?php
					// if current user			 
					if ( $current_user ) {
						echo '<ul class="lms-social-links">';
						    $cryptic_social_facebook = get_the_author_meta( 'smartowl_social_facebook', $user_id );
						    $cryptic_social_twitter = get_the_author_meta( 'smartowl_social_twitter', $user_id );
						    $cryptic_social_linkedin = get_the_author_meta( 'smartowl_social_linkedin', $user_id );
						    $cryptic_social_instagram = get_the_author_meta( 'smartowl_social_instagram', $user_id );
						    $cryptic_social_google_plus = get_the_author_meta( 'smartowl_social_google_plus', $user_id );
						    $instructor_site_url = get_the_author_meta( 'user_url', $user_id );
						     
						    if ( ! empty( $cryptic_social_facebook )) {
						        echo '<li class="lms-facebook"><a href="'.esc_url($cryptic_social_facebook).'"><i class="fa fa-facebook"></i></a></li>';
						    } 
						    if ( ! empty( $cryptic_social_twitter )) {
						        echo '<li class="lms-twitter"><a href="'.esc_url($cryptic_social_twitter).'"><i class="fa fa-twitter"></i></a></li>';
						    }
						    if ( ! empty( $cryptic_social_linkedin )) {
						        echo '<li class="lms-linkedin"><a href="'.esc_url($cryptic_social_linkedin).'"><i class="fa fa-linkedin"></i></a></li>';
						    }
						    if ( ! empty( $cryptic_social_instagram )) {
						        echo '<li class="lms-instagram"><a href="'.esc_url($cryptic_social_instagram).'"><i class="fa fa-instagram"></i></a></li>';
						    }
						    if ( ! empty( $cryptic_social_google_plus )) {
						        echo '<li class="lms-google-plus"><a href="'.esc_url($cryptic_social_google_plus).'"><i class="fa fa-google-plus"></i></a></li>';
						    }
						    if ( ! empty( $instructor_site_url )) {
						        echo '<li class="lms-site-link"><a href="'.esc_url($instructor_site_url).'"><i class="fa fa-link"></i></a></li>';
						    }
						echo '</ul>';
					} ?>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
    </div>
</div>