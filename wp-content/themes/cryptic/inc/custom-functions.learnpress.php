<?php 
if ( !function_exists( 'cryptic_course_details_before_course_tabs' ) ) {
    add_action('learn_press_before_content_landing', 'cryptic_course_details_before_course_tabs');
    add_action('learn_press_before_content_learning', 'cryptic_course_details_before_course_tabs');
    function cryptic_course_details_before_course_tabs() { ?>

        <?php 
            $course = LP()->global['course'];
        ?>
        <div class="course-meta">

            <div class="course-author col-md-3 col-sm-6 col-xs-12" aria-hidden="true" itemprop="author">
                <div class="row">
                    <div class="col-md-3">
                        <img src="<?php echo esc_url(get_template_directory_uri().'/images/lms_icon_3_instructor.png'); ?>" alt="icon">
                    </div>
                    <div class="col-md-9">
                        <h5 class="lms-meta-heading"><?php echo esc_html__('Course Intructor:', 'cryptic'); ?></h5>
                        <?php echo wp_kses_post($course->get_instructor_html()); ?>
                    </div>
                </div>
            </div>

            <div class="course-students col-md-3 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-3">
                        <img src="<?php echo esc_url(get_template_directory_uri().'/images/lms_icon_4_enrolled.png'); ?>" alt="icon">
                    </div>
                    <div class="col-md-9">
                        <h5 class="lms-meta-heading"><?php echo esc_html__('Students Enrolled:', 'cryptic'); ?></h5>
                        <?php echo wp_kses_post($course->get_students_html()); ?>
                    </div>
                </div>
            </div>

            <div class="lms-course-review-stars col-md-3 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-3">
                        <img src="<?php echo esc_url(get_template_directory_uri().'/images/lms_icon_2_reviews.png'); ?>" alt="icon">
                    </div>
                    <div class="col-md-9">
                        <h5 class="lms-meta-heading"><?php echo esc_html__('Course Review:', 'cryptic'); ?></h5>
                        <?php cryptic_course_ratings(); ?>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="row">
                    <div class="col-md-3">
                        <img src="<?php echo esc_url(get_template_directory_uri().'/images/lms_icon_1_status.png'); ?>" alt="icon">
                    </div>
                    <div class="col-md-9 lms-course-price">

                        <?php if ( $course->has_sale_price() ) { ?>
                            <span class="origin-price"><del><?php echo wp_kses_post($course->get_origin_price_html()); ?><del></span>
                        <?php } ?>

                        <?php if ( $course->get_price_html() ) { ?>
                            <span class="price"><?php echo wp_kses_post($course->get_price_html()); ?></span>
                        <?php } ?>

                        <?php do_action( 'learn-press/before-course-buttons' ); ?>
                        <?php
                        /**
                         * @see learn_press_course_purchase_button - 10
                         * @see learn_press_course_enroll_button - 10
                         * @see learn_press_course_retake_button - 10
                         */
                        do_action( 'learn-press/course-buttons' );
                        ?>
                        <?php do_action( 'learn-press/after-course-buttons' ); ?>
                    </div>
                </div>
            </div>
        </div>


        <div class="lms-thumbnail-author-group row">
            <div class="course-thumbnail col-md-9">
                <?php
                global $post;
                $user = get_userdata( get_post_field( 'post_author', $post->id ) );
                $image_title    = esc_attr( get_the_title( get_post_thumbnail_id() ) );
                $image_link     = wp_get_attachment_url( get_post_thumbnail_id() );
                $image          = get_the_post_thumbnail( $post->ID, apply_filters( 'single_course_image_size', 'single_course' ), array(
                    'title' => $image_title,
                    'alt'   => $image_title
                ) );
                ?>
                <?php if(has_post_thumbnail()) : ?>
                    <?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),'cryptic_1000x580' ); ?>
                        <?php if($thumbnail_src) { ?>
                            <img src="<?php echo esc_url($thumbnail_src[0]); ?>" class="img-responsive single-post-featured-img" alt="<?php the_title_attribute(); ?>" />
                        <?php } ?>
                <?php endif; ?>
            </div>
            <div class="col-md-3">
                <div class="lms-course-author-holder">
                    <div class="lms-course-author">
                        <?php 
                        // Instructor avatar
                        if (get_avatar( get_user_meta( $user->ID, $user->user_email, true ) )) {
                            echo get_avatar( $user->ID , 500); 
                        } ?>
                    </div>
                    <div class="text-center">
                        <h3 class="lms-instructor-name"><?php echo wp_kses_post($user->display_name); ?></h3>
                        <?php $user = new WP_User($user->ID); ?>
                        <p class="no-margin role"><?php echo esc_attr(get_user_meta( $user->ID,'cryptic_teacher_function',true )); ?></p>
                        <p class="no-margin"><?php echo esc_attr(get_user_meta( $user->ID, 'description', true )); ?></p>
                        <?php
                            echo '<ul class="lms-social-links">';
                                $cryptic_social_facebook = get_user_meta( $user->ID, 'cryptic_social_facebook', true );
                                $cryptic_social_twitter = get_user_meta( $user->ID, 'cryptic_social_twitter', true );
                                $cryptic_social_linkedin = get_user_meta( $user->ID, 'cryptic_social_linkedin', true );
                                $cryptic_social_instagram = get_user_meta( $user->ID, 'cryptic_social_instagram', true );
                                $cryptic_social_google_plus = get_user_meta( $user->ID, 'cryptic_social_google_plus', true );
                                $instructor_site_url = get_user_meta( $user->ID, 'user_url', true );
                                if ( isset( $cryptic_social_facebook ) && !empty($cryptic_social_facebook)) {
                                    echo '<li class="lms-facebook"><a href="'.esc_url($cryptic_social_facebook).'"><i class="fa fa-facebook"></i></a></li>';
                                } 
                                if ( isset( $cryptic_social_twitter ) && !empty($cryptic_social_twitter)) {
                                    echo '<li class="lms-twitter"><a href="'.esc_url($cryptic_social_twitter).'"><i class="fa fa-twitter"></i></a></li>';
                                }
                                if ( isset( $cryptic_social_linkedin ) && !empty($cryptic_social_linkedin)) {
                                    echo '<li class="lms-linkedin"><a href="'.esc_url($cryptic_social_linkedin).'"><i class="fa fa-linkedin"></i></a></li>';
                                }
                                if ( isset( $cryptic_social_instagram ) && !empty($cryptic_social_instagram)) {
                                    echo '<li class="lms-instagram"><a href="'.esc_url($cryptic_social_instagram).'"><i class="fa fa-instagram"></i></a></li>';
                                }
                            echo '</ul>';
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php 
    }
}
/**
 * Display course info
 */
if ( !function_exists( 'cryptic_lms_course_infos' ) ) {
    function cryptic_lms_course_infos() {
        
        $course = LP()->global['course'];
        $course_id = get_the_ID();
        $lp_duration = get_post_meta( $course_id, '_lp_duration', true );
        $mt_course_language = get_post_meta( $course_id, 'mt_course_language', true );
        ?>
        <div class="lms-course-infos">
            <h3 class="lms-posttitle"><?php esc_html_e( 'Course Features', 'cryptic' ); ?></h3>
            <ul>
                <?php if ( $lp_duration != '' ) { ?>
                    <li>
                        <i class="fa fa-clock-o"></i>
                        <span class="label"><?php esc_html_e( 'Duration', 'cryptic' ); ?></span>
                        <span class="value"><?php echo esc_html( get_post_meta( $course_id, '_lp_duration', true ) ) . esc_attr(' weeks','cryptic'); ?></span>
                    </li>
                <?php } ?>
                <?php if ( $mt_course_language != '' ) { ?>
                    <li>
                        <i class="fa fa-language"></i>
                        <span class="label"><?php esc_html_e( 'Language', 'cryptic' ); ?></span>
                        <span class="value"><?php echo esc_html( get_post_meta( $course_id, 'mt_course_language', true ) ); ?></span>
                    </li>
                <?php } ?>
                <li>
                    <i class="fa fa-users"></i>
                    <span class="label"><?php esc_html_e( 'Students', 'cryptic' ); ?></span>
                    <span class="value"><?php echo wp_kses_post($course->get_students_html()); ?></span>
                </li>
                <li>
                    <i class="fa fa-check-square-o"></i>
                    <span class="label"><?php esc_html_e( 'Assessments', 'cryptic' ); ?></span>
                    <span class="value"><?php echo ( get_post_meta( $course_id, '_lp_course_result', true ) == 'yes' ) ? esc_html__( 'Yes', 'cryptic' ) : esc_html__( 'Self', 'cryptic' ); ?></span>
                </li>
                <?php if(get_the_term_list( $course_id, 'course_category', '', ', ' )){ ?>
                <li>
                    <i class="fa fa-pencil"></i>
                    <span class="label"><?php esc_html_e( 'Categories', 'cryptic' ); ?></span>
                    <span class="value"><?php echo get_the_term_list( $course_id, 'course_category', '', ', ' ); ?></span>
                </li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }
}



/**
 * Display ratings count
 */

if ( !function_exists( 'cryptic_course_ratings_count' ) ) {
    function cryptic_course_ratings_count( $course_id = null ) {
        if ( !class_exists( 'LP_Addon_Course_Review' ) ) {
            return;
        }
        if ( !$course_id ) {
            $course_id = get_the_ID();
        }
        $ratings = learn_press_get_course_rate_total( $course_id ) ? learn_press_get_course_rate_total( $course_id ) : 0;
        echo '<div class="course-comments-count">';
        echo '<div class="value"><i class="fa fa-comment"></i>';
        echo esc_html( $ratings );
        echo '</div>';
        echo '</div>';
    }
}



/**
 * Display course ratings
 */
if ( !function_exists( 'cryptic_course_ratings' ) ) {
    function cryptic_course_ratings() {

        if ( !class_exists( 'LP_Addon_Course_Review' ) ) {
            return;
        }

        $course_id   = get_the_ID();
        $course_rate = learn_press_get_course_rate( $course_id );
        $ratings     = learn_press_get_course_rate_total( $course_id );
        ?>
        <div class="course-review">
            <div class="value">
                <?php cryptic_print_rating( $course_rate ); ?>
                <span class="lms-reviews-number"><?php $ratings ? printf( _n( '(%1$s review)', '(%1$s reviews)', $ratings, 'cryptic' ), number_format_i18n( $ratings ) ) : esc_html_e( '(0 review)', 'cryptic' ); ?></span>
            </div>
        </div>
        <?php
    }
}




if ( !function_exists( 'cryptic_print_rating' ) ) {
    function cryptic_print_rating( $rate ) {
        if ( !class_exists( 'LP_Addon_Course_Review' ) ) {
            return;
        }

        ?>
        <div class="review-stars-rated">
            <ul class="review-stars">
                <li><span class="fa fa-star-o"></span></li>
                <li><span class="fa fa-star-o"></span></li>
                <li><span class="fa fa-star-o"></span></li>
                <li><span class="fa fa-star-o"></span></li>
                <li><span class="fa fa-star-o"></span></li>
            </ul>
            <ul class="review-stars filled" style="<?php echo esc_attr( 'width: ' . ( $rate * 20 ) . '%' ) ?>">
                <li><span class="fa fa-star"></span></li>
                <li><span class="fa fa-star"></span></li>
                <li><span class="fa fa-star"></span></li>
                <li><span class="fa fa-star"></span></li>
                <li><span class="fa fa-star"></span></li>
            </ul>
        </div>
        <?php
    }
}
/**
 * Add some meta data for a course
 *
 * @param $meta_box
 */
if ( !function_exists( 'cryptic_custom_meta_boxes' ) ) {
    function cryptic_custom_meta_boxes( $meta_box ) {
        $fields             = $meta_box['fields'];
        $fields[]           = array(
            'name' => esc_html__( 'Course Language', 'cryptic' ),
            'id'   => 'mt_course_language',
            'type' => 'text',
            'desc' => esc_html__( 'Language\'s used for studying', 'cryptic' ),
            'std'  => esc_html__( 'English', 'cryptic' )
        );
        $meta_box['fields'] = $fields;

        return $meta_box;
    }
}

add_filter( 'learn_press_course_settings_meta_box_args', 'cryptic_custom_meta_boxes' );