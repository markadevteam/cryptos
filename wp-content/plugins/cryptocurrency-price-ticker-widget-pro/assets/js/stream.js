(function ($) {
    'use strict';
    $(document).ready(function(){
        if ( $(".ccpw-live").length>0) {
            $(".ccpw-live").each( function(){
              
            $(this).ccpWsetupWebSocket();
            });
        }
      });
      $.fn.ccpWsetupWebSocket =function(){
          var coinArry=[];
          var thisEle = $(this);
          thisEle.find('.ccpw_coin_cont').each( function(){
             var ContEle =$(this);
             var coin_id=ContEle.data('coin-id');
             coinArry.push(coin_id);
        });
        const pricesWs = new WebSocket('wss://ws.coincap.io/prices?assets='+coinArry.join());
        pricesWs.onmessage = function (msg) {
          var sdata=JSON.parse(msg.data);
    
          for(var indexkey in sdata) {
            if (sdata.hasOwnProperty(indexkey)) {
                var eleHolder= thisEle.find('.ccpw_coin_cont[data-coin-id="' +indexkey+ '"]');  
                if (eleHolder.length>0) {
                    var currency_rate =eleHolder.attr('data-currency-rate');
                    var currency_symbol =eleHolder.attr('data-currency-symbol');
                    var currency_name =eleHolder.attr('data-currency-type');
                    var coinOldPrice=eleHolder.attr('data-coin-price'); 
                    var newPrice=sdata[indexkey]; 
                    var coinLivePrice=newPrice;
                    if (currency_name == "USD") {
                    var converted_price =coinLivePrice;
                    }
                else if (currency_name == "BTC") {
                    if (response.coin != "BTC") {
                        var converted_price =coinLivePrice / currency_rate
                    } else {
                        converted_price = '1.00'
                    }
                }
                else {
                var converted_price =coinLivePrice * currency_rate
                }
                  
                   var formatted_price= ccpwp_numeral_formating(converted_price);
                   if(parseFloat(formatted_price.replace(/,/g , '')) > parseFloat(coinOldPrice)) {
                    eleHolder.addClass("price-plus");
                    eleHolder.attr('data-coin-price',parseFloat(formatted_price.replace(/,/g , '')));
                    eleHolder.find('.live_p').html(currency_symbol+'<span>' + formatted_price + '</span>');
                    //datatable
                    eleHolder.find('.live_price').html(currency_symbol + '<span>' + formatted_price+ '</span>');
                  } else if(parseFloat(formatted_price.replace(/,/g , ''))<parseFloat(coinOldPrice)) {
                    eleHolder.addClass("price-minus");
                    eleHolder.attr('data-coin-price',parseFloat(formatted_price.replace(/,/g , '')));
                    eleHolder.find('.live_p').html(currency_symbol+'<span>' + formatted_price + '</span>');
                    //datatable
                    eleHolder.find('.live_price').html(currency_symbol + '<span>' + formatted_price+ '</span>');
                  } else{
                    //nothing to do
                  }
                   
                }
            }
            setTimeout(function () {
                eleHolder.removeClass('price-plus price-minus');
               }, 200);
        }
        setTimeout(function () {
            eleHolder.removeClass('price-plus price-minus');
           }, 200);
    
    }
   }
    
   function ccpwp_numeral_formating(data){
    if (data >= 25 || data <=-1) {
        var formatedVal = numeral(data).format('0,0.00');
    } else if (data >= 0.50 && data < 25) {
        var formatedVal = numeral(data).format('0,0.000');
    } else if (data >= 0.01 && data < 0.50) {
        var formatedVal = numeral(data).format('0,0.0000');
    } else if (data >= 0.0001 && data < 0.01) {
        var formatedVal = numeral(data).format('0,0.00000');
    } else {
        var formatedVal = numeral(data).format('0,0.00000000');
    } 
    return formatedVal;
}

})(jQuery)