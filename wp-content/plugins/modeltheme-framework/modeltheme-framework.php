<?php
/**
* Plugin Name: ModelTheme Framework
* Plugin URI: http://modeltheme.com/
* Description: ModelTheme Framework required by MODELTHEME Theme.
* Version: 3.0
* Author: ModelTheme
* Author http://modeltheme.com/
* Text Domain: smartowl
*/


$plugin_dir = plugin_dir_path( __FILE__ );





/**
||-> Function: require_once() plugin necessary parts
*/
require_once('inc/post-types/post-types.php'); // POST TYPES
require_once('inc/shortcodes/shortcodes.php'); // SHORTCODES
require_once('inc/widgets/widgets.php'); // WIDGETS
require_once('inc/widgets/widgets-theme.php'); // WIDGETS
require_once('inc/metaboxes/metaboxes.php'); // METABOXES
require_once('inc/metaboxes/metaboxes-taxonomy.php'); // METABOXES FOR TAX's
require_once('inc/demo-importer-v2/wbc907-plugin-example.php');
require_once('inc/mega-menu/mega-menu.php');// MEGA MENU
require_once('inc/sb-google-maps-vc-addon/sb-google-maps-vc-addon.php'); // GMAPS
require_once('inc/custom-functions.php'); // CUSTOM FUNCTIONS


add_action('admin_head', 'modeltheme_disable_notices'); 
function modeltheme_disable_notices() { ?> 
    <style> .notice.is-dismissible { display: none;} </style> 
<?php }

/**

||-> Function: LOAD PLUGIN TEXTDOMAIN

*/
function modeltheme_load_textdomain(){
    $domain = 'modeltheme';
    $locale = apply_filters( 'plugin_locale', get_locale(), $domain );

    load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' );
    load_plugin_textdomain( $domain, FALSE, basename( plugin_dir_path( dirname( __FILE__ ) ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'modeltheme_load_textdomain' );




/**

||-> Function: modeltheme_framework() //renamed from Cryptic v2.6

*/
function modeltheme_framework() {
    // CSS
    wp_register_style( 'style-shortcodes-inc',  plugin_dir_url( __FILE__ ) . 'inc/shortcodes/shortcodes.css' );
    wp_enqueue_style( 'style-shortcodes-inc' );
    wp_register_style( 'style-mt-mega-menu',  plugin_dir_url( __FILE__ ) . 'css/mt-mega-menu.css' );
    wp_enqueue_style( 'style-mt-mega-menu' );
    wp_register_style( 'style-select2',  plugin_dir_url( __FILE__ ) . 'css/select2.min.css' );
    wp_enqueue_style( 'style-select2' );
    wp_register_style( 'style-animations',  plugin_dir_url( __FILE__ ) . 'css/animations.css' );
    wp_enqueue_style( 'style-animations' );
    
    // SCRIPTS
    wp_enqueue_script( 'js-modeltheme-featured-house-modernizr-custom', plugin_dir_url( __FILE__ ) . 'js/mt-featured-house/modernizr-custom.js', array(), '1.0.0', true );
    wp_enqueue_script( 'classie', plugin_dir_url( __FILE__ ) . 'js/classie.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'js-mt-plugins', plugin_dir_url( __FILE__ ) . 'js/mt-plugins.js', array(), '1.0.0', true );
    wp_enqueue_script( 'percircle', plugin_dir_url( __FILE__ ) . 'js/mt-skills-circle/percircle.js', array(), '1.0.0', true );
    wp_enqueue_script( 'select2', plugin_dir_url( __FILE__ ) . 'js/select2.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'particles', plugin_dir_url( __FILE__ ) . 'js/mt-particles/particles.min.js', array(), '2.0.0', true );
    wp_enqueue_script( 'particles-app', plugin_dir_url( __FILE__ ) . 'js/mt-particles/app.js', array(), '1.0.0', true );
    wp_enqueue_script( 'statsjs', plugin_dir_url( __FILE__ ) . 'js/mt-particles/stats.js', array(), '2.0.0', true );
    wp_enqueue_script( 'modeltheme-custom', plugin_dir_url( __FILE__ ) . 'js/modeltheme-custom.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'magnific-popup', plugin_dir_url( __FILE__ ) . 'js/mt-video/jquery.magnific-popup.js', array(), '1.0.0', true );
    wp_enqueue_script( 'hammer', plugin_dir_url( __FILE__ ) . 'js/hammer.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'jquery-waypoint', plugin_dir_url( __FILE__ ) . 'js/jquery-waypoint.js', array(), '1.0.0', false );
    wp_enqueue_script( 'lottie-web', plugin_dir_url( __FILE__ ) . 'js/lottie-web.js', array(), '1.0.0', false );
    wp_enqueue_script( 'js-modeltheme-featured-house-main', plugin_dir_url( __FILE__ ) . 'js/mt-featured-house/main.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'js-modeltheme-mt-coundown-version2', plugin_dir_url( __FILE__ ) . 'js/mt-coundown-version2/flipclock.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'livecoin', plugin_dir_url( __FILE__ ) . 'js/livecoin.js', array(), '1.0.0', true );
    
}
add_action( 'wp_enqueue_scripts', 'modeltheme_framework' );




/**

||-> Function: modeltheme_enqueue_admin_scripts()

*/
function modeltheme_enqueue_admin_scripts( $hook ) {
    // JS
    wp_enqueue_script( 'js-modeltheme-admin-custom', plugin_dir_url( __FILE__ ) . 'js/modeltheme-custom-admin.js', array(), '1.0.0', true );
    // CSS
    wp_register_style( 'css-modeltheme-custom',  plugin_dir_url( __FILE__ ) . 'css/modeltheme-custom.css' );
    wp_enqueue_style( 'css-modeltheme-custom' );
    wp_register_style( 'css-fontawesome-icons',  plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css' );
    wp_enqueue_style( 'css-fontawesome-icons' );
    wp_register_style( 'css-simple-line-icons',  plugin_dir_url( __FILE__ ) . 'css/simple-line-icons.css' );
    wp_enqueue_style( 'css-simple-line-icons' );
    wp_register_style( 'cryptocoins-icons',  plugin_dir_url( __FILE__ ) . 'fonts/cryptocoins.css' );
    wp_enqueue_style( 'cryptocoins-icons' );

}
add_action('admin_enqueue_scripts', 'modeltheme_enqueue_admin_scripts');




    
    

add_image_size( 'mt_1250x700', 1250, 700, true );
add_image_size( 'mt_320x480', 320, 480, true );
add_image_size( 'mt_900x550', 900, 550, true );




/**

||-> Function: modeltheme_cmb_initialize_cmb_meta_boxes

*/
function modeltheme_cmb_initialize_cmb_meta_boxes() {
    if ( ! class_exists( 'cmb_Meta_Box' ) )
        require_once ('init.php');
}
add_action( 'init', 'modeltheme_cmb_initialize_cmb_meta_boxes', 9999 );



/**

||-> Function: modeltheme_cmb_initialize_cmb_meta_boxes

*/
function modeltheme_excerpt_limit($string, $word_limit) {
    $words = explode(' ', $string, ($word_limit + 1));
    if(count($words) > $word_limit) {
        array_pop($words);
    }
    return implode(' ', $words);
}



// WP 5 UPDATE ======================================================================================================
// WP 5 UPDATE ======================================================================================================
// WP 5 UPDATE ======================================================================================================


// |---> REDUX FRAMEWORK
if (!function_exists('cryptic_RemoveDemoModeLink')) {
    function cryptic_RemoveDemoModeLink() { // Be sure to rename this function to something more unique
        if ( class_exists('ReduxFrameworkPlugin') ) {
            remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
        }
        if ( class_exists('ReduxFrameworkPlugin') ) {
            remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );    
        }
    }
    add_action('init', 'cryptic_RemoveDemoModeLink');
}


/* transactions */
if(!function_exists('cryptic_crypto_transactions')) { 
    function cryptic_crypto_transactions($atts){
        $content = '';
        $content .= '
                    <div class="crypto-tools-par">
                        <h1>'.esc_html__('Bitcoin Explorer','modeltheme').'</h1>
                        <p>'.esc_html__('Explore Bitcoin Addresses, Hashes and Transactions codes with only one click.','modeltheme').'</p>
                        <form class="transactions-search" method="GET" action="">
                            <input type="text" placeholder="'.esc_attr__('Block, Hash, Transaction, etc...','modeltheme').'" name="">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <p class="fullwidth-paragraph">
                        <strong>'.esc_html__('Block Example:','modeltheme').'</strong> 514392<br />
                        <strong>'.esc_html__('Address/Hash Example:','modeltheme').'</strong> 16GSaYFj4njEqWcFz4coQGzNrvZs9MUJm7<br />
                        <strong>'.esc_html__('Transaction Example:','modeltheme').'</strong> 0121e4ea10d8b4535e8bd93fd3915cbf80a3c6cd8a01a9cf37e0cc34e1979b9e</p>
                    </div>';
        if (strpos($_SERVER['REQUEST_URI'], "block") !== false){
            $blockGet = $_GET["block"];
            $content .= cryptic_get_block($blockGet);
        } elseif(strpos($_SERVER['REQUEST_URI'], "btc-address") !== false) {
            $addressGet = $_GET["btc-address"];
            $content .= cryptic_get_address($addressGet);
        } elseif(strpos($_SERVER['REQUEST_URI'], "tx") !== false) {
            $transactionGet = $_GET["tx"];
            $content .= cryptic_get_transactions($transactionGet);
        } elseif(strpos($_SERVER['REQUEST_URI'], "?") !== false) {
            $content .='<div class="alert alert-danger">'.esc_html__('The block, hash, transaction, etc is incorrect.','modeltheme').'</div>';
        }
        return $content;
    }
    add_shortcode( 'crypto-tools-transactions', 'cryptic_crypto_transactions' );
}

if(!function_exists('cryptic_get_block')) {
    function cryptic_get_block($blockGet) {
        $block_explorer = 'https://blockchain.info/block-height/'.esc_attr($blockGet).'?format=json';
        $block_explorer_content = @file_get_contents($block_explorer);
        $block = json_decode($block_explorer_content, true);
        $block2 = json_decode($block_explorer_content, true);
        if ($block != null) {
            $content = '';
            if (isset($_GET['block'])) {
                $content .= '<div class="crypto-tools-search-content">';
                    $content .= '<h2 class="title">'.esc_html__('Block # ','modeltheme').esc_attr($blockGet).'</h2>';
                    $content .= '<div class="row">';
                        $content .= '<div class="col-md-8">';
                            $content .= '<h3 class="subtitle">'.esc_html__('Summary:','modeltheme').'</h3>';
                            $content .= '<ul>';
                            foreach($block['blocks'] as $block){
                                $content .= '<li>'.esc_html__('Number Of Transactions: ','modeltheme') . esc_attr($block['n_tx']) . '</li>';
                                $content .= '<li>'.esc_html__('Transaction Fees: ','modeltheme'). number_format(esc_attr($block['fee']) / 100000000, 8, '.', ',') .esc_html__(' BTC','modeltheme').'</li>';
                                $content .= '<li>'.esc_html__('Transaction Fees: ','modeltheme') . number_format(esc_attr($block['fee']) / 100000000, 8, '.', ',') .esc_html__(' BTC','modeltheme').'</li>';
                                $content .= '<li>'.esc_html__('Height: ','modeltheme') . '<a href="?block='.esc_attr($block['height']).'">' . esc_attr($block['height']) . '</a></li>';
                                $content .= '<li>'.esc_html__('Timestamp: ','modeltheme') . gmdate("Y-m-d (H:i:s)", esc_attr($block['time'])) . '</li>';
                                $content .= '<li>'.esc_html__('Received Time: ','modeltheme') . gmdate("Y-m-d (H:i:s)", esc_attr($block['received_time'])) . '</li>';
                                $content .= '<li>'.esc_html__('Size: ','modeltheme') . esc_attr($block['size']) . '</li>';
                            }
                            $content .= '</ul>';
                        $content .= '</div>';                
                        $content .= '<div class="col-md-4">';
                            $content .= '<h3 class="subtitle">'.esc_html__('Hashes','modeltheme').'</h3>';
                            $content .= '<ul>';
                            foreach($block2['blocks'] as $block){
                                $content .= '<li>'.esc_html__('Hash: ','modeltheme') . esc_attr($block['hash']) . '</li>';
                                $content .= '<li>'.esc_html__('Previous Block: ','modeltheme') . esc_attr($block['prev_block']) . '</li>';
                                $content .= '<li>'.esc_html__('Merkle Root: ','modeltheme') . esc_attr($block['mrkl_root']) . '</li>';
                            }
                            $content .= '</ul>';
                        $content .= '</div>';
                    $content .= '</div>';
                $content .= '</div>';
            }
        }else{
            $content .= '<div class="alert alert-danger">'.esc_html__('The block is incorrect. Try again using a correct/real block id!','modeltheme').'</div>';
        }
        return $content;
    }
}

if(!function_exists('cryptic_get_address')) {
    function cryptic_get_address($addressGet) {
        $address_explorer = 'https://blockchain.info/rawaddr/'.esc_attr($addressGet).'?format=json';
        $address_explorer_content = @file_get_contents($address_explorer);
        $address = json_decode($address_explorer_content, true);
        $content = '';
        if ($address != null) {
            if (isset($_GET['btc-address'])) {
                $content .= '<div class="crypto-tools-search-content">';
                    $content .= '<h2 class="title">'.esc_html__('Bitcoin Address  - Addresses are identifiers which you use to send bitcoins to another person.','modeltheme').'</h2>';
                    $content .= '<h3 class="subtitle">'.esc_html__('Summary: ','modeltheme').'</h3>';
                    $content .= '<ul>';
                        $content .= '<li>'.esc_html__('Address: ','modeltheme').'<a href="?btc-address='.esc_attr($address['address']).'">' . esc_attr($address['address']) . '</a></li>';
                        $content .= '<li>'.esc_html__('Hash 160: ','modeltheme') . esc_attr($address['hash160']) . '</li>';
                        $content .= '<li>'.esc_html__('No. Transactions: ','modeltheme') . esc_attr($address['n_tx']) . '</li>';
                        $content .= '<li>'.esc_html__('Total Received: ','modeltheme'). number_format(esc_attr($address['total_received']) / 100000000, 6, '.', ',') . ' BTC</li>';
                        $content .= '<li>'.esc_html__('Total Sent: ','modeltheme') . number_format(esc_attr($address['total_sent']) / 100000000, 6, '.', ',') . ' BTC</li>';
                        $content .= '<li>'.esc_html__('Final Balance: ','modeltheme') . number_format(esc_attr($address['final_balance']) / 100000000, 0, '.', ',') . ' BTC</li>';
                    $content .= '</ul>';
                    $content .= '<h2 class="title">'.esc_html__('Transactions List: ','modeltheme').'</h3>';
                    $content .= '<ul>';
                        foreach($address['txs'] as $singletransaction){
                            $content .= '<div class="single-transaction">';
                                $content .= '<h3 class="subtitle"><a href="?tx='.esc_attr($singletransaction['hash']).'">' . esc_attr($singletransaction['hash']) . '</a></h3>';
                                if ($singletransaction['out']) {
                                    $content .= '<ul class="transaction-list">';
                                        foreach($singletransaction['out'] as $inputs){
                                            if ($inputs['addr'] == $_GET['btc-address']) {                                           
                                                $content .= '<li class="addr1">
                                                    <a href="?btc-address='.esc_attr($inputs['addr']).'">' . esc_attr($inputs['addr']) . ' </a>
                                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                    <a href="?btc-address='.esc_attr($address['address']).'">' . esc_attr($address['address']) . '</a>
                                                    <span class="value">' . number_format(esc_attr($inputs['value']) / 100000000, 6, '.', ',') .esc_html__(' BTC','modeltheme').'</span>
                                                </li>';
                                            } else {
                                                $content .= '<li class="addr2">
                                                    <a href="?btc-address='.esc_attr($address['address']).'">' . esc_attr($address['address']) . '</a> 
                                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i> 
                                                    <a href="?btc-address='.esc_attr($inputs['addr']).'">' . esc_attr($inputs['addr']) . ' </a>
                                                    <span class="value">' . number_format(esc_attr($inputs['value']) / 100000000, 6, '.', ',') .esc_html__(' BTC','modeltheme').'</span>
                                                </li>';
                                            }
                                        }
                                    $content .= '</ul>';
                                }
                            $content .= '</div>';
                        }   
                    $content .= '</ul>';
                $content .= '</div>';
            }
        }else{
            $content .= '<div class="alert alert-danger">'.esc_html__('The block is incorrect. Try again using a correct/real block id!','modeltheme').'</div>';
        }
        return $content;
    }
}

if(!function_exists('cryptic_get_transactions')) {
    function cryptic_get_transactions($transactionGet) {
        $transactions = 'https://blockchain.info/rawtx/'.esc_attr($transactionGet).'?format=json';
        $transactions_content = @file_get_contents($transactions);
        $transaction = json_decode($transactions_content, true);
        $content = '';
        if ($transaction != null) {
            $content .= '<div class="crypto-tools-search-content">';
                $content .= '<h2 class="title">'.esc_html__('Transaction - View information about a bitcoin transaction','modeltheme').'</h2>';
                $content .= '<h3 class="subtitle">'.esc_html__('Currenct Transaction: ','modeltheme').'<a href="?tx='.esc_attr($transactionGet).'">' . esc_attr($transactionGet) . '</a></h3>';
                if (isset($_GET['tx'])) {
                    foreach($transaction['inputs'] as $inputs){
                        $content .= '<h3 class="subtitle">'.esc_html__('Bitcoin Address: ','modeltheme'). '<a href="?btc-address='.esc_attr($inputs['prev_out']['addr']).'">' . esc_attr($inputs['prev_out']['addr']) . '</a></h3>';
                    }  
                    $content .= '<div class="row transaction-info">';
                        $content .= '<div class="col-md-6">';
                            $content .= '<h3 class="subtitle">'.esc_html__('Summary','modeltheme') .'</h3>';
                            if (isset($_GET['tx'])) {
                                $content .= '<ul>';
                                    $content .= '<li>' .esc_html__('Size: ','modeltheme') . esc_attr($transaction['size']) . ' (bytes)</li>';
                                    $content .= '<li>' .esc_html__('Weight: ','modeltheme') . '</li>';
                                    $content .= '<li>' .esc_html__('Received time: ','modeltheme') . gmdate("Y-m-d (H:i:s)", esc_attr($transaction['time'])) . '</li>';
                                    $content .= '<li>' .esc_html__('Included Blocks: ','modeltheme') . esc_attr($transaction['block_height']) . '">' . esc_attr($transaction['block_height']) . '</a></li>';
                                $content .= '</ul>';
                            }
                        $content .= '</div>';  
                        $content .= '<div class="col-md-6">';
                            $content .= '<h3 class="subtitle">'.esc_html__('Inputs and Outputs','modeltheme').'</h3>';
                            if (isset($_GET['tx'])) {
                                $content .= '<ul>';
                                    foreach($transaction['inputs'] as $inputs){
                                        $content .= '<li>'.esc_html__('Current Address: ','modeltheme').'<a href="?btc-address='.esc_attr($inputs['prev_out']['addr']).'">' . esc_attr($inputs['prev_out']['addr']) . '</a></li>';
                                        $content .= '<li>'.esc_html__('Total Input: ','modeltheme') . number_format(esc_attr($inputs['prev_out']['value']) / 100000000, 8, '.', ',') .esc_html__(' BTC','modeltheme').'</li>';
                                        $content .= '<li>'.esc_html__('Total Output: ','modeltheme'). number_format(esc_attr($inputs['prev_out']['value']) / 100000000, 8, '.', ',') .esc_html__(' BTC','modeltheme').'</li>';
                                    }   
                                $content .= '</ul>';
                            }
                        $content .= '</div>'; 
                    $content .= '</div>'; 
                    $content .= '<ul>';
                    foreach($transaction['out'] as $singletransaction){
                        $content .= '<li> '.esc_html__('To Address: ','modeltheme'). '<a href="?btc-address='.esc_attr($singletransaction['addr']).'">' . esc_attr($singletransaction['addr']) . '</a></li>';
                        $content .= '<li>'.esc_html__('Value: ','modeltheme'). esc_attr($singletransaction['value']) / 100000000 .esc_html__(' BTC','modeltheme').'</li>';
                    }   
                    $content .= '</ul>';
                }
            $content .= '</div>';
        }else{
            $content .= '<div class="alert alert-danger">'.esc_html__('The transaction is incorrect. Try again using a correct/real transaction!','modeltheme').'</div>';
        }

        return $content;
    }
}

if(!function_exists('cryptic_crypto_transactions_premium')) {
    function cryptic_crypto_transactions_premium() {
        add_rewrite_endpoint( 'crypto-tools-transactions', EP_ROOT | EP_PAGES );
    }
    add_action( 'init', 'cryptic_crypto_transactions_premium' );
}

if(!function_exists('cryptic_crypto_transactions_premium_vars')) {
    function cryptic_crypto_transactions_premium_vars( $vars ) {
        $vars[] = 'crypto-transactions';
        return $vars;
    }
    add_filter( 'query_vars', 'cryptic_crypto_transactions_premium_vars', 0 );
}

if(!function_exists('cryptic_crypto_transactions_premium_content')) {
    function cryptic_crypto_transactions_premium_content() {
        echo do_shortcode( '[crypto-tools-transactions]' );
    }
    add_action( 'woocommerce_account_crypto-tools-transactions_endpoint', 'cryptic_crypto_transactions_premium_content' );
}



/* Woocommerce new tab */ 
if(!function_exists('cryptic_crypto_tools_content')) {
    function cryptic_crypto_tools_content($atts){
        $content = '';

        $slides = cryptic_redux('slides-crypto-tools');
        if(!empty($slides)) {
            foreach($slides as $slide) {

                $crypto_tools_title = '';
                if(!empty($slide['title'])) {
                    $crypto_tools_title = $slide['title'];
                }
                $crypto_tools_description = '';
                if(!empty($slide['description'])) {
                    $crypto_tools_description = $slide['description'];
                }
                $crypto_tools_url = '';
                if(!empty($slide['url'])) {
                    $crypto_tools_url = $slide['url'];
                }
                $crypto_tools_image = '';
                if(!empty($slide['image'])) {
                    $crypto_tools_image = $slide['image'];
                }

                $content .= '<div class="crypto-tools-parent currency-exchange">';
                    $content .= '<div class="crypto-tools-image">';
                        $content .= '<a href="'.esc_attr($crypto_tools_url).'"><img src="'.esc_attr($crypto_tools_image).'"></a>';
                    $content .= '</div>';
                    $content .= '<div class="crypto-tools-content">';
                        $content .= '<a href="'.esc_attr($crypto_tools_url).'"><h3 class="crypto-tools-title">'.esc_attr($crypto_tools_title).'</h3></a>';
                        $content .= '<p class="crypto-tools-paragraph">'.esc_attr($crypto_tools_description).'</p>';
                    $content .= '</div>';
                    $content .= '<div class="crypto-tools-button">';
                        $content .= '<a href="'.esc_attr($crypto_tools_url).'"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>';
                    $content .= '</div>';
                    $content .= '<div class="clearfix"></div>';
                $content .= '</div>';


            }
        }
        $switch = cryptic_redux('switch-slides-crypto');
        if($switch == 1) {
            return $content;
        } else {
            $content_off = '<a class="back-home" href="'.esc_url(site_url()).'">'.esc_html__('Back to homepage','modeltheme').'</a>';
            return $content_off;
        }

    }
    add_shortcode( 'crypto_tools', 'cryptic_crypto_tools_content' );
}


if(!function_exists('cryptic_my_login_form_shortcode')) {
    function cryptic_my_login_form_shortcode() {

        $content = '';
        if ( is_user_logged_in() ) {

            $content .= '<script>
            window.location="'.get_site_url().'/my-account/crypto-tools/";
            </script>';

            return $content;
            
        } else {
            $content .= '<div class="login-register-shortcode">';
                $content .= '<div id="login-content-shortcode">';
                    $content .= '<h3 class="relative">'.esc_html__('Login to Your Account','modeltheme').'</h3>';
                    $content .= '<div class="alert alert-success alert-dismissible cryptic-demo-login">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                          <strong>Username:</strong> cryptic_demo<br>
                          <strong>Password:</strong> cryptic_demo
                    </div>';
                    $content .= '<div class="login-content-row row">';
                        $content .= '<div class="col-md-12">';
                            $content .= wp_login_form(array( 'echo' => false ));
                            if ( class_exists( 'UM' ) ) {
                                $content .= '<a class="btn btn-register-shortcode" id="register-btn-shortcode">'.esc_html__('Register','modeltheme').'</a>';
                            }
                        $content .= '</div>';
                    $content .= '</div>';
                $content .= '</div>';
                $content .= '<div id="register-content-shortcode">';
                    $content .= '<h3 class="relative">'.esc_html__('Personal Details','modeltheme').'</h3>';
                    $content .= '<div class="register-content-row row">';
                        $content .= '<div class="col-md-12">';
                            $content .= do_shortcode('[ultimatemember form_id=7]');
                        $content .= '</div>';
                    $content .= '</div>';
                $content .= '</div>';
            $content .= '</div>';
            return $content;
        }
    }
    add_shortcode( 'login-form', 'cryptic_my_login_form_shortcode' );
}


?>