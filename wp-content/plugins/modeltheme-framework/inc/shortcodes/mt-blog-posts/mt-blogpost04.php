<?php 


/**

||-> Shortcode: BlogPost04

*/
function modeltheme_shortcode_blogpost04($params, $content) {
    extract( shortcode_atts( 
        array(
            'animation'           =>'',
            'number'              =>'',
            'content_color'       =>'',
            'content_color_hover' =>'',
            'link_view_more'      =>''
        ), $params ) );


    $uniqid = 'unique'.uniqid();

    $html = '';
    $html .= '<div class="blog-posts blog-posts4 blog-posts4-'.$uniqid. ' simple-posts blog-posts-shortcode wow '.$animation.'">';
    $html .= '<div class="row">';
    $args_blogposts = array(
            'posts_per_page'   => $number,
            'orderby'          => 'post_date',
            'order'            => 'DESC',
            'post_type'        => 'post',
            'post_status'      => 'publish' 
            ); 

    $html .= '<style> .blog-posts4-'.$uniqid.' .list-view .post-details .post-name a:hover,
                      .blog-posts4-'.$uniqid.' .text-element.content-element .author a:hover { color:'.$content_color_hover.'!important; } </style>';

    $blogposts = get_posts($args_blogposts);

    foreach ($blogposts as $blogpost) {


        #thumbnail
        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $blogpost->ID ),'cryptic_news_shortcode3_800x500' );
        
        $content_post   = get_post($blogpost->ID);
        $content        = $content_post->post_content;
        $content        = apply_filters('the_content', $content);
        $content        = str_replace(']]>', ']]&gt;', $content);

        $postslider_id  = $blogpost->ID;
        $post_author_id = get_post_field( 'post_author', $postslider_id );

        if ($thumbnail_src) {
            $post_img = '<img class="blog_post_image" src="'. esc_url($thumbnail_src[0]) . '" alt="'.$blogpost->post_title.'" />';
            $post_col = 'col-md-7';
        }else{
            $post_col = 'col-md-7 no-featured-image';
            $post_img = '';
        }

        $comments_count = wp_count_comments($blogpost->ID);
        if($comments_count->approved >= 1) {
            $comments = 'Comments <a href="'.get_comments_link($blogpost->ID).'">'. $comments_count->approved.'</a>';
        } else {
            $comments = 'No comments';
        }

        $html.='<div class="col-md-12">
                  <article class="single-post list-view">
                    <div class="blog_custom">
                      <!-- POST THUMBNAIL -->
                      <div class="col-md-5 post-thumbnail">
                          <a class="relative" href="'.get_permalink($blogpost->ID).'">'.$post_img.'</a>
                      </div>
                      <!-- POST DETAILS -->
                      <div class="post-details '.$post_col.'">
                      <div class="title-n-excerpt">
                        <h3 class="post-name row">
                          <a  style="color:'.$content_color.'" href="'.get_permalink($blogpost->ID).'" title="'. $blogpost->post_title .'">'. $blogpost->post_title .'</a>
                        </h3>
                        <div class="post-excerpt row">
                            <p>'.modeltheme_excerpt_limit($content, 15).'</p>
                        </div>
                      </div>
                        <div class="text-element content-element">
                          <h5 style="color:'.$content_color.'"><span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> '.get_the_date('M j, Y', $blogpost->ID).'</span>
                            <span class="author">'.esc_html__('by','modeltheme').' <strong><a href="'.get_author_posts_url($post_author_id).'">'.get_the_author_meta( 'display_name' , $post_author_id ).'</a></strong></span>
                          </h5>
                        </div>
                      </div>                      
                    </div>
                  </article>
                </div>';
      }

      $html .= '<div class="col-md-12 view-more"><a href="'.esc_attr($link_view_more).'">'.esc_html__('View more posts','modeltheme').'</a></div>';

    $html .= '</div>';
    $html .= '</div>';
    return $html;
}
add_shortcode('blogpost04', 'modeltheme_shortcode_blogpost04');

/**

||-> Map Shortcode in Visual Composer with: vc_map();

*/
if ( is_plugin_active( 'js_composer/js_composer.php' ) ) {

    require_once __DIR__ . '/../vc-shortcodes.inc.arrays.php';

  	$post_category_tax = get_terms('category');
  	$post_category = array();
  	foreach ( $post_category_tax as $term ) {
  		  $post_category[$term->name] = $term->slug;
  	}

    vc_map( array(
     "name" => esc_attr__("MT - Blog Posts version 4", 'modeltheme'),
     "base" => "blogpost04",
     "category" => esc_attr__('MT: ModelTheme', 'modeltheme'),
     "icon" => "smartowl_shortcode",
     "params" => array(
        array(
          "group" => "Options",
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__( "Number of posts", 'modeltheme' ),
          "param_name" => "number",
          "value" => "",
          "description" => esc_attr__( "Enter number of blog post to show.", 'modeltheme' )
        ),
        array(
          "group" => "Options",
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__( "Link for View More Button", 'modeltheme' ),
          "param_name" => "link_view_more",
          "value" => "",
          "description" => esc_attr__( "Enter link for View More Button", 'modeltheme' )
        ),
        array(
          "group" => "Styling",
          "type" => "colorpicker",
          "class" => "",
          "heading" => esc_attr__( "Choose content color", 'modeltheme' ),
          "param_name" => "content_color",
          "value" => '', //Default color
          "description" => esc_attr__( "Choose content color", 'modeltheme' )
        ),
        array(
          "group" => "Styling",
          "type" => "colorpicker",
          "class" => "",
          "heading" => esc_attr__( "Choose content color on hover", 'modeltheme' ),
          "param_name" => "content_color_hover",
          "value" => '', //Default color
          "description" => esc_attr__( "Choose content color on hover", 'modeltheme' )
        ),
        array(
          "group" => "Animation",
          "type" => "dropdown",
          "heading" => esc_attr__("Animation", 'modeltheme'),
          "param_name" => "animation",
          "std" => 'fadeInLeft',
          "holder" => "div",
          "class" => "",
          "description" => "",
          "value" => $animations_list
        )
      )
  ));
}

?>