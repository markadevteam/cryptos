<?php
/**

||-> Shortcode: Livecoin

*/

require_once(__DIR__.'/../vc-shortcodes.inc.arrays.php');

//require_once(__DIR__.'/mt-livecoin-function.php');


function modeltheme_livecoin_shortcode($params, $content) {
    extract( shortcode_atts( 
        array(
          'ico_name'                      => '',
          'content_color'                 => '',
          'background_box'                => '',
          'animation'                     => ''
        ), $params ) ); 

        $uniqid = 'unique'.uniqid();
 
        $content .= '<div id="widget-livecoin" class="livecoin-box '.$uniqid.' wow '.$animation.'" style="color:'.esc_attr($content_color).';background-color:'.esc_attr($background_box).'" data-currencyid="'.esc_attr($ico_name).'" data-base="USD" data-secondary="" data-ticker="true" data-rank="true" data-marketcap="true" data-volume="true" data-stats="USD" data-statsticker="false"></div>';
        return $content;

}       
add_shortcode('livecoin', 'modeltheme_livecoin_shortcode');


/**

||-> Map Shortcode in Visual Composer with: vc_map();

*/
if ( is_plugin_active( 'js_composer/js_composer.php' ) ) {

  #SHORTCODE: Livecoin
  vc_map( array(
     "name" => esc_attr__("MT - Livecoin Box", 'modeltheme'),
     "base" => "livecoin",
     "category" => esc_attr__('MT: ModelTheme', 'modeltheme'),
     "icon" => "smartowl_shortcode",
     "params" => array(
         array(
            "type" => "dropdown",
            "heading" => esc_attr__("Coin name", 'modeltheme'),
            "param_name" => "ico_name",
            "std" => '',
            "holder" => "div",
            "class" => "",
            "description" => "",
            "value" => $coins_list,
            "group" => "Livecoin box setup"
        ),
        array(
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Color of content", 'modeltheme' ),
            "param_name" => "content_color",
            "value" => "",
            "group" => "Livecoin Style"
         ),
         array(
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => esc_attr__( "Background of the box", 'modeltheme' ),
            "param_name" => "background_box",
            "value" => "",
            "group" => "Livecoin Style"
        ),
        array(
            "type" => "dropdown",
            "heading" => esc_attr__("Animation", 'modeltheme'),
            "param_name" => "animation",
            "std" => '',
            "holder" => "div",
            "class" => "",
            "description" => "",
            "value" => $animations_list,
            "group" => "Livecoin box Animation"
        ),
     )
  ));
}

?>