<?php 
require_once(__DIR__.'/../vc-shortcodes.inc.arrays.php');



class Modeltheme_Toggle {

    protected $_items_per_row;

    /**
     * Get things started
     */
    public function __construct() {

        add_shortcode('mt_toggle_short', array($this, 'mt_shortcode_toggle'));
        add_shortcode('mt_toggle_short_item', array($this, 'mt_shortcode_toggle_items'));
        add_action('init', array($this, 'map_vc_element'));
        add_action('init', array($this, 'map_child_vc_element'));

    }

    /**
    ||-> Shortcode: toggle
    */
    public function mt_shortcode_toggle($params,  $content = NULL) {
        extract( shortcode_atts( 
            array(
                'el_class'              			=>'',
                'items_per_row'                     =>''
            ), $params ) );

        $this->_items_per_row = $items_per_row;

        $html = '';
        $counter_parent = 0;

        $html .= '<div class="toggle-shortcode parent-'.$counter_parent.' naccs '.$el_class.' row">';
            $html .= '<div class="grid-div">';
                	$html .= do_shortcode($content);
            $html .= '</div>';
        $html .= '</div>';
        ++$counter_parent;
        
        return $html;
    }

    /**
    ||-> Shortcode: Child Shortcode v1
    */
    public function mt_shortcode_toggle_items($params, $content = NULL) {
        static $counter = 0;
        extract( shortcode_atts( 
            array(
                'toggle_item_title'                 => '',
                'toggle_item_content'               => '',
                'toggle_item_class'                 => '',
                'toggle_item_image'              	=> '',
                'toggle_item_title_color'          	=> '',
                'items_per_row'						=> 'items_per_row',
                'items_per_row_new'					=> 'items_per_row_new'
            ), $params ) );

        $items_per_rowP = $this->_items_per_row;

        $thumb      = wp_get_attachment_image_src($toggle_item_image, "full");
        $thumb_src  = $thumb[0];

        // custom javascript
        $html .= '<script>
                    function toggleIcon(e) {
                        jQuery(e.target)
                            .prev(".panel-heading-toggle")
                    }
                    jQuery(".panel-group-toggle").on("hidden.bs.collapse", toggleIcon);
                    jQuery(".panel-group-toggle").on("shown.bs.collapse", toggleIcon);
                  </script>';

        $html = '';
        // $html .= '<div class="container">';
        $html .= '<div class="panel-group-toggle '.$items_per_rowP.'" role="tablist" aria-multiselectable="true">';
            $html .= '<div class="panel-toggle panel-default-toggle '.$toggle_item_class.'">';
                $html .= '<div class="panel-heading-toggle collapsed" role="tab" id="heading-'.$counter.'">';
                    $html .= '<div class="panel-title-toggle">';
                        $html .= '<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-'.$counter.'" aria-expanded="true" aria-controls="collapse-'.$counter.'">';
                            $html .= '<span class="toggle-item-image"><img src="'.esc_attr($thumb_src).'" data-src="'.esc_attr($thumb_src).'"></span>';
                            $html .= '<span style="color: '.esc_attr($toggle_item_title_color).'">'.$toggle_item_title.'</span>';
                        $html .= '</a>';
                    $html .= '</div>';
                $html .= '</div>';
                $html .= '<div class="panel-collapse collapse" id="collapse-'.$counter.'" role="tabpanel" aria-labelledby="heading-'.$counter.'">';
                    $html .= '<div class="panel-body-toggle">';
                          $html .= '<p>'.$toggle_item_content.'</p>';
                    $html .= '</div>';
                $html .= '</div>';
            $html .= '</div>';
        $html .= '</div>';
        // $html .= '</div>';
        $counter++;
        return $html;
    }

    /**
    ||-> Map Shortcode in Visual Composer with: vc_map();
    */
    function map_vc_element(){
        if (function_exists("vc_map")) {
            vc_map( array(
                "name" => esc_attr__("MT - Toggle", 'modeltheme'),
                "base" => "mt_toggle_short",
                "as_parent" => array('only' => 'mt_toggle_short_item'), // Use only|except attributes to limit child shortcodes (separate multiple values with comma)
                "content_element" => true,
                "show_settings_on_create" => true,
                "icon" => "smartowl_shortcode",
                "category" => esc_attr__('MT: ModelTheme', 'modeltheme'),
                "is_container" => true,
                "params" => array(
                    // add params same as with any other content element
                    array(
        	            "group" => "General Options",
        	            "type" => "dropdown",
        	            "holder" => "div",
        	            "class" => "",
        	            "heading" => esc_attr__("Toggle Items Per Row", 'mtlistings'),
        	            "param_name" => "items_per_row",
        	            "std" => '',
        	            "description" => "",
        	            "value" => array(
        	                esc_attr__('1 Item/Row', 'mtlistings')         => 'col-md-12',
        	                esc_attr__('2 Items/Row', 'mtlistings')         => 'col-md-6',
        	                esc_attr__('3 Items/Row', 'mtlistings')         => 'col-md-4',
        	                esc_attr__('4 Items/Row', 'mtlistings')         => 'col-md-3',
        	            )
        	        ),
                    array(
                    	"group" => "General Options",
                        "type" => "textfield",
                        "heading" => __("Extra class name", "modeltheme"),
                        "param_name" => "el_class",
                        "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "modeltheme")
                    ),
                ),
                "js_view" => 'VcColumnView'
            ) );
        }
    }
    function map_child_vc_element(){
        if (function_exists("vc_map")) {
            vc_map( array(
                "name" => esc_attr__("Toggle Item", 'modeltheme'),
                "base" => "mt_toggle_short_item",
                "content_element" => true,
                "as_child" => array('only' => 'mt_toggle_short'), // Use only|except attributes to limit parent (separate multiple values with comma)
                "params" => array(
                    // add params same as with any other content element
                    array(
        		        "group" 	   => "General Options",
        		        "type" 		   => "attach_images",
        		        "holder"       => "div",
        		        "class"        => "",
        		        "heading"      => esc_attr__( "Choose image", 'modeltheme' ),
        		        "param_name"   => "toggle_item_image",
        		        "value" 	   => "",
        		        "description"  => esc_attr__( "Choose image for toggle", 'modeltheme' )
        		        ),
                    array(
                        "group"        => "General Options",
                        "type"         => "textfield",
                        "holder"       => "div",
                        "class"        => "",
                        "param_name"   => "toggle_item_title",
                        "heading"      => esc_attr__("Single item Toggle Title", 'modeltheme'),
                        "description"  => esc_attr__("Enter title for current toggle item.", 'modeltheme'),
                    ),
                    array(
                        "group"        => "General Options",
                        "type"         => "textarea_html",
                        "holder"       => "div",
                        "class"        => "",
                        "param_name"   => "toggle_item_content",
                        "heading"      => esc_attr__("Single item toggle Description", 'modeltheme'),
                        "description"  => esc_attr__("Enter the description for current toggle item.", 'modeltheme'),
                    ),
                    array(
                        "group"        => "General Options",
                        "type" 		   => "textfield",
                        "heading"      => __("Add class active for first element", "modeltheme"),
                        "param_name"   => "toggle_item_class",
                        "description"  => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "modeltheme")
                    ),
                    array(
        		        "group"        => "Styling",
        		        "type"         => "colorpicker",
        		        "class"        => "",
        		        "heading"      => esc_attr__( "Single item Toggle Title Color", 'modeltheme' ),
        		        "param_name"   => "toggle_item_title_color",
        		        "value"        => "", //Default color
        		        "description"  => esc_attr__( "Choose text color", 'modeltheme' )
        	        ),
                )
            ) );
        }
    }
}


//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Mt_toggle_Short extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Mt_toggle_Short_Item extends WPBakeryShortCode {
    }
}

// Initialize Element Class
if (class_exists('Modeltheme_Toggle')) {
    new Modeltheme_Toggle();
}
?>