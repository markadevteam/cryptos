<?php

require_once(__DIR__.'/../vc-shortcodes.inc.arrays.php');

/**

||-> Shortcode: Post Slider

*/

function modeltheme_shortcode_posts_slider($params, $content) {
    extract( shortcode_atts( 
        array(
            'animation'             =>'',
            'post_slider_color'   =>'',
            'number'                =>'',
            'visible_items'         =>''
        ), $params ) );




    $html = '';
    $html .= '<div class="vc_row">';
        $html .= '<div class="wow '.$animation.' post-slider-container-'.$visible_items.' owl-carousel owl-theme">';
        $args_postslider = array(
                'posts_per_page'   => $number,
                'orderby'          => 'post_date',
                'order'            => 'DESC',
                'post_type'        => 'post',
                'post_status'      => 'publish' 
                ); 
        $postslider = get_posts($args_postslider);
            foreach ($postslider as $post) {
                $postslider_id  = $post->ID;
                $categories = get_the_category($post->ID);//$post->ID

                $post_author_id = get_post_field( 'post_author', $postslider_id );
                
                $html.='
                  <div class="item vc_col-md-12 relative">
                    <div class="post_slider_item">
                        <div class="categories">';

                          foreach($categories as $category){
                              $category_link = get_category_link($category->cat_ID);
                              $html.='<a href="'.esc_url($category_link).'">'.$category->cat_name.' </a>';
                          }

                        $html.='</div>
                      <h2 class="name-test"><strong><a href="'.get_the_permalink($post->ID).'">'. $post->post_title .'</a></strong></h2>
                      <h5><span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> '.get_the_date('M j, Y', $post->ID).'</span>
                          <span class="author">'.esc_html__('by','modeltheme').' <a href="'.get_author_posts_url($post_author_id).'">'.get_the_author_meta( 'display_name' , $post_author_id ).'</a></span>
                      </h5>
                    </div>
                  </div>';

            }
    $html .= '</div>
      </div>';

    $html .= '</div>
    <style type="text/css" scoped>
      p.position-test {
          color: '.esc_attr($post_slider_color).';
      }
    </style>';
    return $html;

}
add_shortcode('post_slider', 'modeltheme_shortcode_posts_slider');



/**

||-> Map Shortcode in Visual Composer with: vc_map();

*/
if ( is_plugin_active( 'js_composer/js_composer.php' ) ) {

    vc_map( array(
     "name" => esc_attr__("MT - Post Slider", 'modeltheme'),
     "base" => "post_slider",
     "category" => esc_attr__('MT: ModelTheme', 'modeltheme'),
     "icon" => "smartowl_shortcode",
     "params" => array(
        array(
          "group" => "Options",
          "type" => "textfield",
          "holder" => "div",
          "class" => "",
          "heading" => esc_attr__( "Number of Posts", 'modeltheme' ),
          "param_name" => "number",
          "value" => "",
          "description" => esc_attr__( "Enter number of posts to show.", 'modeltheme' )
        ),
        array(
          "group" => "Options",
          "type" => "dropdown",
          "heading" => esc_attr__("Visible Posts per slide", 'modeltheme'),
          "param_name" => "visible_items",
          "std" => '',
          "holder" => "div",
          "class" => "",
          "description" => "",
          "value" => array(
            '1'   => '1',
            '2'   => '2',
            '3'   => '3'
            )
        ),
        array(
          "group" => "Styling",
          "type" => "colorpicker",
          "class" => "",
          "heading" => esc_attr__( "Post color", 'modeltheme' ),
          "param_name" => "post_slider_color",
          "value" => "", //Default color
          "description" => esc_attr__( "Choose Post color", 'modeltheme' )
        ),
        array(
          "group" => "Animation",
          "type" => "dropdown",
          "heading" => esc_attr__("Animation", 'modeltheme'),
          "param_name" => "animation",
          "std" => 'fadeInLeft',
          "holder" => "div",
          "class" => "",
          "description" => "",
          "value" => $animations_list
        )
      )
  ));
}

?>